%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Implementation of the Finite Element Method for the Helmholtz Problem %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
clc
close all

% Starting parameters
a    = -1;       % Left-end point        
b    =  1;       % Right-end point
x0   =  (a+b)/2; % Parameter bilinear form
beta = 100;%(b-a)/2;  % Parameter bilinear form
A_bl = 1/3;      % Parameter bilinear form
k    = 10;        % Frequency
N = 8;          % N: number of nodes, 2N: number of basis fcts


% Input data
f = @(x)  1;
g_plus  = 0;
g_minus = 0;


% Basis Nodes : x_1 to x_N
table_nodes = zeros(N,1);
for p=1:N
    table_nodes(p) = a + (p-1)*(b-a)/(N-1);
end

% Construction of the stiffness matrix: Define (A)ij = a(phi_j,phi_i)
A = zeros(2*N);
for idx1=1:2*N  
    for idx2=1:2*N
        if abs(ceil(idx1/2) - ceil(idx2/2)) < 2
            A(idx2,idx1) = sesform(idx1,idx2,table_nodes,a,b,k,x0,beta,A_bl);           
        end
    end
end 
A = sparse(A);

% Construction of the right-hand side: Calculate c in A*x_sol = c, c_i = l(phi_i)
l_vec = zeros(2*N,1);
for i=1:2*N
    l_vec(i) = rhs(i,table_nodes,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);
end

% Solve for coefs in A*coefs=l_vec
coefs = real(A\l_vec);

coefs1 = femRunner(table_nodes,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%        True Solution            %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define true solution u(x) for f=1:
u_g = @(x) (1/(2*k^2))*(-2 + exp(-1i*k*(x-a)) + exp(1i*k*(x-b)));
w = @(x) (-exp(1i*k)/(2i*k)).*(g_minus*exp(1i*k*x) + g_plus*exp(-1i*k*x));
u = @(x) u_g(x) + w(x);

% Alternative
% homogenenous
w1 = @(x) w(x);
% particular
fun0 = @(x,s) (1/2*1i*k)*exp(1i*k*abs(x-s)) .* f(s);
fun1 = @(x,s) (1/2*1i*k)*exp(1i*k*(x-s)) .* f(s);
fun2 = @(x,s) (1/2*1i*k)*exp(1i*k*(s-x)) .* f(s);
u_g0 = @(x) arrayfun(@(x)integral(@(s)fun0(x,s),a,b,'ArrayValued',true), x);
u_g1 = @(x) arrayfun(@(x)integral(@(s)fun1(x,s),a,x,'ArrayValued',true), x);
u_g2 = @(x) arrayfun(@(x)integral(@(s)fun2(x,s),x,b,'ArrayValued',true), x);

% complete
u_0 = @(x) u_g0(x);
u1 = @(x) ( u_g1(x) + u_g2(x))/k^2;

% Plot approximation and true solution
x = linspace(a,b,300);
y = zeros(length(x), 1);
y1 = zeros(length(x), 1);

figure()
hold on
for ix = 1 : length(x)
    %y(ix) = real(approx_sol(x(ix), coefs, table_nodes));
    y1(ix) = real(approx_sol(x(ix), coefs1, table_nodes));
end
%plot(x,y,'DisplayName', 'Approx.')
plot(x,y1,'--', 'DisplayName', 'Approx.')
plot(x, real(u(x)), 'DisplayName', 'Target');
plot(x, real(u1(x)), 'DisplayName', 'ALternative Target');
plot(x, real(u_0(x)),'--', 'DisplayName', 'Abs alternative');
title('FEM with cubic polynomials basis')
subtitle("f(x)=1, interval:(" + a+","+b+"), k="+k+", n="+N)
legend show
axis padded
grid on