function [res] = femRunner(table_nodes,a,b,k,x0,beta,A_bl,f,g_plus,g_minus)
%%% Returns coefficients from fem for given node mesh

% INPUT:  
%         table_nodes,  vector of size n+1,
%         R,            Proportion of nodes to add
%         a,            left-end point,
%         b,            right-end point,
%         k,            frequency,
%         x0,           parameter of bilinear form,
%         beta,         parameter of bilinear form,
%         A,            parameter of bilinear form,
% OUTPUT: index of node left and right of new node

% 
% % Starting parameters
% a    = -1;       % Left-end point        
% b    =  1;       % Right-end point
% x0   =  (a+b)/2; % Parameter bilinear form
% beta = 100;%(b-a)/2;  % Parameter bilinear form
% A_bl = 1/3;      % Parameter bilinear form
% k    = 10;        % Frequency
 N = length(table_nodes);    % N: number of nodes, 2N: number of basis fcts
% 
% % Input data
% f = @(x)  1;
% g_plus  = 0;
% g_minus = 0;

% Construction of the stiffness matrix: Define (A)ij = a(phi_j,phi_i)
A = zeros(2*N);
for idx1=1:2*N  
    for idx2=1:2*N
        if abs(ceil(idx1/2) - ceil(idx2/2)) < 2
            A(idx2,idx1) = sesform(idx1,idx2,table_nodes,a,b,k,x0,beta,A_bl);           
        end
    end
end 
A = sparse(A);

% Construction of the right-hand side: Calculate c in A*x_sol = c, c_i = l(phi_i)
l_vec = zeros(2*N,1);
for i=1:2*N
    l_vec(i) = rhs(i,table_nodes,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);
end

% Solve for coefs in A*coefs=l_vec
coefs = A\l_vec;
res = real(coefs);

end
