function [res] = sesformOGA(x_pre1,x_post1,k1,x_pre2,x_post2,k2,coef_idx1,coefs,a,b,k,x0,beta,A)
% INPUT:  
%         idx1,2        index of basis function (1,2*N)
%         table_nodes,  vector of size n+1,
%         a,            left-end point,
%         b,            right-end point,
%         k,            frequency,
%         x0,           parameter of bilinear form,
%         beta,         parameter of bilinear form,
%         A,            parameter of bilinear form,
% OUTPUT: Bilinear form a(u,v), u=phi_idx1, v=phi_idx2


% first function (approximation)
u = @(x) coefs(coef_idx1) * basis(x, x_pre1, x_post1, k1);
u_prime = @(x) coefs(coef_idx1) * basis_prime(x, x_pre1, x_post1, k1);
u_prime_prime = @(x) coefs(coef_idx1) * basis_prime_prime(x, x_pre1, x_post1, k1);

% second function (new basis)
v = @(x) basis(x, x_pre2, x_post2, k2);
v_prime = @(x) basis_prime(x, x_pre2, x_post2, k2);
v_prime_prime = @(x) basis_prime_prime(x, x_pre2, x_post2, k2);

   
% Morawetz and Helmholtz Operators
M_u = @(x) (x-x0).*u_prime(x)-1i*k*beta*u(x);
M_v = @(x) (x-x0).*v_prime(x)-1i*k*beta*v(x);
L_u = @(x) u_prime_prime(x) + k^2 * u(x);
L_v = @(x) v_prime_prime(x) + k^2 * v(x);

% Computation
lowerbound = max(x_pre1,x_pre2);
upperbound = min(x_post1,x_post2);

first_integrand = @(x) u_prime(x).*conj(v_prime(x));
first = integral(first_integrand,lowerbound,upperbound,'ArrayValued', true);

second_integrand = @(x) u(x).*conj(v(x));
second = integral(second_integrand,lowerbound,upperbound,'ArrayValued',true);

third_integrand = @(x) (M_u(x) + (A*L_u(x)/k^2)).*conj(L_v(x));
third = integral(third_integrand,lowerbound,upperbound, 'ArrayValued',true);

fourth = 0;
fifth  = 0;
sixth  = 0;
seventh = 0;

%fourth = -1i*k*(conj(M_v(b)).*u(b) + conj(M_v(a)).*u(a));
%fifth = -M_u(b).*conj(v_prime(b)) + M_u(a).*conj(v_prime(a));
%sixth =  - (b-x0).*u(b).*conj(v(b)) + (a-x0).*u(a).*conj(v(a));
%seventh = + (b-x0).*u_prime(b).*conj(v_prime(b)) - (a-x0).*u_prime(a).*conj(v_prime(a));

res = first + (k^2)*second + third + fourth + fifth + (k^2)*sixth + seventh;
   
end
   
        
        
        
    
    




