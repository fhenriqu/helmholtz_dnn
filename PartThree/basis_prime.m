function [out] = basis_prime(x, x_i, x_post, k)
% INPUT:  x,      value to evaluate 
%         x_i,    first node of interval I_i
%         x_post, last node of interval
%         k,      index of hermite basis polynomial: theta_j
% OUTPUT: derivative of the hermite basis polynomial scaled to element I_i

h = x_post - x_i;
t = (x - x_i)./h;

if k == 1
    out = (2 * (t-1).^2 + 2*(2*t+1).*(t-1))./h ;
elseif k == 2
    out = (t-1).^2 + 2*t.*(t-1);
elseif k == 3
    out = (-2*t.^2 + 2*(3-2*t).*t)./h;
elseif k == 4
    out = t.^2 + 2*(t-1).*t ;
end

