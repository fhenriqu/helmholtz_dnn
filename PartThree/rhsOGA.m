function [res] = rhsOGA(x_pre,x_mid,x_post,k,x0,beta,A,f)
% INPUT:  
%         a,             Left-end point,
%         b,             Right-end point,
%         k,             Frequency,
%         x0,            Parameter of bilinear form,
%         beta,          Parameter of bilinear form,
%         A,          Parameter of bilinear form,
%         f,             Function handler for source term,
%         g_plus,        Impedance boundary condition at x=a,
%         g_minus,       Impedance boundary condition at x=b,


%%% Construction of basis functions %%%
% Left interval (x_pre, x_post)
vL1 = @(x) basis(x, x_pre, x_mid, 3);
vL1_prime = @(x) basis_prime(x, x_pre, x_mid, 3);
vL1_prime_prime = @(x) basis_prime_prime(x, x_pre, x_mid, 3);

vL2 = @(x) basis(x, x_pre, x_mid, 4);
vL2_prime = @(x) basis_prime(x, x_pre, x_mid, 4);
vL2_prime_prime = @(x) basis_prime_prime(x, x_pre, x_mid, 4);
   
% right interval (x_mid, x_post)
vR1 = @(x) basis(x, x_mid, x_post, 1);
vR1_prime = @(x) basis_prime(x, x_mid, x_post, 1);
vR1_prime_prime = @(x) basis_prime_prime(x, x_mid, x_post, 1);

vR2 = @(x) basis(x, x_mid, x_post, 2);
vR2_prime = @(x) basis_prime(x, x_mid, x_post, 2);
vR2_prime_prime = @(x) basis_prime_prime(x, x_mid, x_post, 2);


% Morawetz and Helmholtz Operators
% Left interval 
M_vL1 = @(x) (x-x0).*vL1_prime(x)-1i*k*beta*vL1(x);
M_vL2 = @(x) (x-x0).*vL2_prime(x)-1i*k*beta*vL2(x);
L_vL1 = @(x) vL1_prime_prime(x) + k^2 * vL1(x);
L_vL2 = @(x) vL2_prime_prime(x) + k^2 * vL2(x);

% Right interval 
M_vR1 = @(x) (x-x0).*vR1_prime(x)-1i*k*beta*vR1(x);
M_vR2 = @(x) (x-x0).*vR2_prime(x)-1i*k*beta*vR2(x);
L_vR1 = @(x) vR1_prime_prime(x) + k^2 * vR1(x);
L_vR2 = @(x) vR2_prime_prime(x) + k^2 * vR2(x);


%%%% LEFT INTERVAL %%%
% First contribution
firstintL1 = @(x) (conj(M_vL1(x))-A*conj(L_vL1(x))/(k^2)).*f(x);
firstL1  = integral(firstintL1,x_pre,x_mid,'ArrayValued', true);

firstintL2 = @(x) (conj(M_vL2(x))-A*conj(L_vL2(x))/(k^2)).*f(x);
firstL2  = integral(firstintL2,x_pre,x_mid,'ArrayValued', true);


%%% RIGHT INTERVAL %%%
% First contribution
firstintR1 = @(x) (conj(M_vR1(x))-A*conj(L_vR1(x))/(k^2)).*f(x);
firstR1  = integral(firstintR1,x_mid, x_post,'ArrayValued', true);

firstintR2 = @(x) (conj(M_vR2(x))-A*conj(L_vR2(x))/(k^2)).*f(x);
firstR2  = integral(firstintR2,x_mid, x_post,'ArrayValued', true);


res = firstL1 + firstL2 + firstR1 + firstR2;

end
