function [res] = calcL2(table_nodes, coefs, k, f, g_plus, g_minus, a, b)
% INPUT:  
%         table_nodes,      vector of node values, (N,1)
%         coefs,            Coefficients of basis functions, (2N,1)
% OUTPUT: L2 error of the approximation


%%%%%% Define analytical solution %%%%%% 

% homogenenous
w = @(x) (-1/(2*k^2))*(g_minus*exp(1i*k*x) + g_plus*exp(-1i*k*x));

% particular
fun = @(x,s) (1/2*1i*k)*exp(1i*k*abs(x-s)) .* f(s);
u_g = @(x) arrayfun(@(x)integral(@(s)fun(x,s),a,b,'ArrayValued',true), x);

% complete
u = @(x) w(x) + u_g(x)/(k^2);
     
            
%%% Compute error
N = length(table_nodes);
I = 0;

for i=1:N-1
    x_pre = table_nodes(i);
    x_post = table_nodes(i+1);
    
    approx = @(x) coefs(2*i-1) * basis(x, x_pre, x_post, 1) ...
                + coefs(2)     * basis(x, x_pre, x_post, 2) ...
                + coefs(2*i+1) * basis(x, x_pre, x_post, 3) ...
                + coefs(2*i+2) * basis(x, x_pre, x_post, 4) ;
    
    product = @(x) (real(u(x) - approx(x))).^2;
            
    int = integral(product, x_pre+eps, x_post-eps);
    I = I + int;
end

res = I;

end