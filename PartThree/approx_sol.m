
function res = w_n(x, coefs, table_nodes)
% INPUT: 
%         x,            value to evaluate at
%         coefs,        coefs obtained from FEM,
%         table_nodes,  vector of size N,
% OUTPUT: FEM approximation at x

N = length(table_nodes);
temp = 0;
for i=1:2*N 
    node_idx = ceil(i/2);
    
    % first two basis functions
    if (i==1) || (i==2)
        x_pre = table_nodes(1);
        x_post = table_nodes(2); 
        
        if (x < x_pre) || (x_post < x)
            continue  
        else
            if (mod(i,2) == 1)
                temp = temp + coefs(i)*basis(x, x_pre, x_post, 1);
            else
                temp = temp + coefs(i)*basis(x, x_pre, x_post, 2);
            end
        end
    
    % Last two basis functions
    elseif (i==2*N-1) || (i==2*N)
        x_pre = table_nodes(N-1);
        x_post = table_nodes(N);
     
        if (x < x_pre) || (x_post < x)
            continue          
        else
            if (mod(i,2) == 1)
                temp = temp + coefs(i)*basis(x, x_pre, x_post, 3);
            else
                temp = temp + coefs(i)*basis(x, x_pre, x_post, 4);
            end
        end
        
    % intermediary basis function    
    else
        x_pre = table_nodes(node_idx-1);
        x_mid = table_nodes(node_idx);
        x_post = table_nodes(node_idx+1);
            
        if (x < x_pre) || (x_post < x)
            continue
        elseif (x_pre <= x) && (x <= x_mid)
            if mod(i,2) == 1
                temp = temp + coefs(i)*basis(x, x_pre, x_mid, 3);
            else
                temp = temp + coefs(i)*basis(x, x_pre, x_mid, 4);
            end
        elseif (x_mid < x) && (x <= x_post)
            if mod(i,2) == 1
                temp = temp + coefs(i)*basis(x, x_mid, x_post, 1);
            else
                temp = temp + coefs(i)*basis(x, x_mid, x_post, 2);
            end
        end
        
    end
end
res = temp;
end
        
        
        
       