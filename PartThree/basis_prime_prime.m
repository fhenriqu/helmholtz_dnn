function [out] = basis_prime(x, x_i, x_post, k)
% INPUT:  x,      value to evaluate 
%         x_i,    first node of interval I_i
%         x_post, last node of interval
%         k,      index of hermite basis polynomial: theta_j
% OUTPUT: derivative of the hermite basis polynomial scaled to element I_i

h = x_post - x_i;
t = (x - x_i)./h;

if k == 1
    out = (12*t - 6)./ (h^2);
elseif k == 2
    out = (6*t - 4) ./ h;
elseif k == 3
    out = (-12*t + 6) ./ (h^2);
elseif k == 4
    out = (6*t - 2) ./ h;
end

