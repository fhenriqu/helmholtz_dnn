clc
clear 
close all

% Starting parameters
a    = -1;       % Left-end point        
b    =  1;       % Right-end point
x0   =  (a+b)/2; % Parameter bilinear form
beta = 100; %(b-a)/2;  % Parameter bilinear form
A_bl = 1/3;      % Parameter bilinear form
k    = 10;        % Frequency
max_N = 16;      % Maximum number of nodes
R = 0.2;         % Refinement factor


% Input data
f = @(x) sqrt(abs(x));
g_plus  = 0;
g_minus = 0;


%%%%%%%%%%%%%%%       TRUE SOLUTION        %%%%%%%%%%%%%%%%%%%%
% homogenenous
w = @(x) (-1/(2*k^2))*(g_minus*exp(1i*k*x) + g_plus*exp(-1i*k*x));

% particular
fun = @(x,s) (1/2*1i*k)*exp(1i*k*abs(x-s)) .* f(s);
u_g = @(x) arrayfun(@(x)integral(@(s)fun(x,s),a,b,'ArrayValued',true), x);

% complete
u = @(x) w(x) + (u_g(x))/k^2;

% % Define true solution u(x) for f=1:
% u_g = @(x) (1/(2*k^2))*(-2 + exp(1i*k*(x+1)) + exp(-1i*k*(x-1)));
% w = @(x) (-exp(1i*k)/(2i*k)).*(g_minus*exp(1i*k*x) + g_plus*exp(-1i*k*x));
% u = @(x) u_g(x) + w(x);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%         EQUIDISTANT NODES         %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
max_i = floor(log2(max_N))-1;
node_numbers_equi = zeros(max_i,1);
L2_equi  = zeros(max_i,1);

% Plot 
figure(2)
hold on

x = linspace(a,b,300);
y = zeros(length(x),1);

for i=1:max_i
    N = 2^(i+1);
    node_numbers_equi(i) = N;
    
    % form table
    table_nodes_equi = zeros(N,1);
    for p=1:N
        table_nodes_equi(p) = a + (p-1)*(b-a)/(N-1);
    end
     
    % calculate fem coefficients
    coefs_equi = femRunner(table_nodes_equi,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);
    
    % calculate errors
    L2_equi(i) = calcL2(table_nodes_equi,coefs_equi,k,f,g_plus,g_minus,a,b);
    
    % Plot 
    for ix = 1 : length(x)
        y(ix) = real(approx_sol(x(ix), coefs_equi, table_nodes_equi));
    end
    
    plot(x,y,'--','DisplayName', "N ="+N)
    
end
plot(x, real(u(x)),'r','linewidth',0.7, 'DisplayName', 'Target')
title('Equidistant Nodes')
legend show
axis padded
grid on


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%       Refined Mesh            %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
j = 4;

% Calulate number of iterations
n_iter = 0;
while(j < max_N)
    n_iter = n_iter + 1;
    j = ceil(j*(1+R));
end


% Compute nodes and respective coefs and errors
j = 4;
% Initialize table of nodes:
table_nodes_pre = zeros(j,1);
for p=1:j           
    table_nodes_pre(p) = a + (p-1)*(b-a)/(j-1);
end


L2_refined = zeros(n_iter,1);
nodes_numbers = zeros(n_iter,1);
temp = 1;

% Plot
figure(3)
hold on
y1 = y;

for i=1:n_iter
    nodes_numbers(temp) = j;
    
    if (j == 4)
        table_nodes_post = table_nodes_pre
    else
        table_nodes_post = NewNodes(table_nodes_pre,R,coefs_post,a,b,k,x0,beta,A_bl,f)
    end
        
    % calculate fem coefficients
    coefs_post = femRunner(table_nodes_post,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);
        
    % calculate errors
    L2_refined(i) = calcL2(table_nodes_post,coefs_post,k,f,g_plus,g_minus,a,b);
    
    % plot 
    for ix = 1 : length(x)
        y(ix) = real(approx_sol(x(ix), coefs_post, table_nodes_post));
    end
    plot(x,y,'--','DisplayName', "N ="+j)
    
    % update values 
    table_nodes_pre = table_nodes_post;    
    j = ceil(j*(1+R));
    temp = temp + 1;
       
end

plot(x,real(u(x)), 'r', 'linewidth', 0.7, 'DisplayName','Target')    
title('Refined Mesh')
legend show
axis padded
grid on
node_numbers_equi
L2_equi

nodes_numbers
L2_refined


% %%%%%%%%%%%%%%%%%%%%  Plotting  %%%%%%%%%%%%%%%%%%%
% figure(4)
% loglog(node_numbers_equi, L2_equi,'-o', 'DisplayName', 'Equidistant')
% hold on
% loglog(nodes_numbers, L2_refined,'-o', 'DisplayName', 'Refined: R=0.3')
% xlabel('log N')
% ylabel('log L2 Error')
% title('L2 Error vs Mesh Size, Cubic FEM')
% subtitle({'u(x)=sqrt(|x|), interval:(-1,1), k='+k})
% legend show
% grid on


figure
loglog(node_numbers_equi, L2_equi, '-o', 'DisplayName', 'Equidistant')
hold on
loglog(nodes_numbers, L2_refined,'-o', 'DisplayName', 'Refined: R=0.5')
xlabel('log N')
ylabel('log L2 Error')
title('L2 Error vs Mesh Size, Cubic FEM')
%subtitle({'f(x)=1, interval:(-1,1), k='+k})
legend show
axis padded
grid on


p = polyfit(log(node_numbers_equi), log(L2_equi), 1);
slope_L2_equi = p(1)
p = polyfit(log(nodes_numbers), log(L2_refined), 1);
slope_L2_refined = p(1)


