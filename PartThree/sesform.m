function [res] = sesform(idx1,idx2,table_nodes,a,b,k,x0,beta,A)
% INPUT:  
%         idx1,2        index of basis function (1,2*N)
%         table_nodes,  vector of size n+1,
%         a,            left-end point,
%         b,            right-end point,
%         k,            frequency,
%         x0,           parameter of bilinear form,
%         beta,         parameter of bilinear form,
%         A,            parameter of bilinear form,
% OUTPUT: Bilinear form a(u,v), u=phi_idx1, v=phi_idx2

N = length(table_nodes);
bnode_1 = ceil(idx1/2); % Node center of first function v 
bnode_2 = ceil(idx2/2); % Node center of second functin u

if abs(bnode_1 - bnode_2) >= 2
    res = 0;
    return
end 

%%% DIFFERENT INTERVAL %%%
if abs(bnode_1 - bnode_2) == 1
    x_pre = table_nodes(min(bnode_1, bnode_2));
    x_post = table_nodes(max(bnode_1, bnode_2));
    
    % define basis functions
    if bnode_1 < bnode_2
        if mod(idx1, 2) == 1
            u = @(x) basis(x, x_pre, x_post, 1);
            u_prime = @(x) basis_prime(x, x_pre, x_post, 1);
            u_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 1);
        else    
            u = @(x) basis(x, x_pre, x_post, 2);
            u_prime = @(x) basis_prime(x, x_pre, x_post, 2);
            u_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 2);
        end
        
        if mod(idx2, 2) == 1
            v = @(x) basis(x, x_pre, x_post, 3);
            v_prime = @(x) basis_prime(x, x_pre, x_post, 3);
            v_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 3);
        else    
            v = @(x) basis(x, x_pre, x_post, 4);
            v_prime = @(x) basis_prime(x, x_pre, x_post, 4);
            v_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 4);
        end
        
    elseif bnode_1 > bnode_2
        
        if mod(idx1, 2) == 1
            u = @(x) basis(x, x_pre, x_post, 3);
            u_prime = @(x) basis_prime(x, x_pre, x_post, 3);
            u_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 3);
        else    
            u = @(x) basis(x, x_pre, x_post, 4);
            u_prime = @(x) basis_prime(x, x_pre, x_post, 4);
            u_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 4);
        end
        
        if mod(idx2, 2) == 1
            v = @(x) basis(x, x_pre, x_post, 1);
            v_prime = @(x) basis_prime(x, x_pre, x_post, 1);
            v_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 1);    
        else      
            v = @(x) basis(x, x_pre, x_post, 2);
            v_prime = @(x) basis_prime(x, x_pre, x_post, 2);
            v_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 2);
        end
    end
    
    % Morawetz and Helmholtz Operators
    M_u = @(x) (x-x0).*u_prime(x)-1i*k*beta*u(x);
    M_v = @(x) (x-x0).*v_prime(x)-1i*k*beta*v(x);
    L_u = @(x) u_prime_prime(x) + k^2 * u(x);
    L_v = @(x) v_prime_prime(x) + k^2 * v(x);
    
    lowerbound = x_pre;
    upperbound = x_post;

    first_integrand = @(x) u_prime(x).*conj(v_prime(x));
    first = integral(first_integrand,lowerbound,upperbound,'ArrayValued', true);

    second_integrand = @(x) u(x).*conj(v(x));
    second = integral(second_integrand,lowerbound,upperbound,'ArrayValued',true);

    third_integrand = @(x) (M_u(x) + (A*L_u(x)/k^2)).*conj(L_v(x));
    third = integral(third_integrand,lowerbound,upperbound, 'ArrayValued',true);
    
    fourth = 0;
    fifth  = 0;
    sixth  = 0;
    seventh = 0;
          
    %fourth = -1i*k*(conj(M_v(b)).*u(b) + conj(M_v(a)).*u(a));
    %fifth = -M_u(b).*conj(v_prime(b)) + M_u(a).*conj(v_prime(a));
    %sixth =  - (b-x0).*u(b).*conj(v(b)) + (a-x0).*u(a).*conj(v(a));
    %seventh = + (b-x0).*u_prime(b).*conj(v_prime(b)) - (a-x0).*u_prime(a).*conj(v_prime(a));

    res = first + (k^2)*second + third + fourth + fifth + (k^2)*sixth + seventh;
    return
    
end

%%% SAME INTERVAL %%%
    
% first interval
if (bnode_1==bnode_2) && ((bnode_1==1) || (bnode_1==N))
    if (bnode_1==1)

        x_pre = table_nodes(1);
        x_post = table_nodes(2);

        % define basis functions
        if idx1 == 1
            u = @(x) basis(x, x_pre, x_post, 1);
            u_prime = @(x) basis_prime(x, x_pre, x_post, 1);
            u_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 1);
        elseif idx1 == 2
            u = @(x) basis(x, x_pre, x_post, 2);
            u_prime = @(x) basis_prime(x, x_pre, x_post, 2);
            u_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 2);
        end

        if idx2 == 1
            v = @(x) basis(x, x_pre, x_post, 1);
            v_prime = @(x) basis_prime(x, x_pre, x_post, 1);
            v_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 1);
        elseif idx2 == 2
            v = @(x) basis(x, x_pre, x_post, 2);
            v_prime = @(x) basis_prime(x, x_pre, x_post, 2);
            v_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 2);
        end

% last interval : node N
    elseif (bnode_1==N)

        x_pre = table_nodes(N-1);
        x_post = table_nodes(N);

        % define basis functions
        if idx1 == 2*N - 1
            u = @(x) basis(x, x_pre, x_post, 3);
            u_prime = @(x) basis_prime(x, x_pre, x_post, 3);
            u_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 3);
        elseif idx1 == 2*N
            u = @(x) basis(x, x_pre, x_post, 4);
            u_prime = @(x) basis_prime(x, x_pre, x_post, 4);
            u_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 4);
        end

        if idx2 == 2*N - 1
            v = @(x) basis(x, x_pre, x_post, 3);
            v_prime = @(x) basis_prime(x, x_pre, x_post, 3);
            v_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 3);
        elseif idx2 == 2*N
            v = @(x) basis(x, x_pre, x_post, 4);
            v_prime = @(x) basis_prime(x, x_pre, x_post, 4);
            v_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 4);
        end
    end
    
    % Morawetz and Helmholtz Operators
    M_u = @(x) (x-x0).*u_prime(x)-1i*k*beta*u(x);
    M_v = @(x) (x-x0).*v_prime(x)-1i*k*beta*v(x);
    L_u = @(x) u_prime_prime(x) + k^2 * u(x);
    L_v = @(x) v_prime_prime(x) + k^2 * v(x);
    
    % Computation
    lowerbound = x_pre;
    upperbound = x_post;

    first_integrand = @(x) u_prime(x).*conj(v_prime(x));
    first = integral(first_integrand,lowerbound,upperbound,'ArrayValued', true);

    second_integrand = @(x) u(x).*conj(v(x));
    second = integral(second_integrand,lowerbound,upperbound,'ArrayValued',true);

    third_integrand = @(x) (M_u(x) + (A*L_u(x)/k^2)).*conj(L_v(x));
    third = integral(third_integrand,lowerbound,upperbound, 'ArrayValued',true);
    
    if (bnode_1 == 1) && (bnode_2 == 1)
        fourth = -1i*k*conj(M_v(a)).*u(a);
        fifth  = M_u(a).*conj(v_prime(a));
        sixth  = (a-x0).*u(a).*conj(v(a));
        seventh = -(a-x0).*u_prime(a).*conj(v_prime(a));
    elseif (bnode_1 == N) && (bnode_2 == N)
        fourth = -1i*k*conj(M_v(b)).*u(b);
        fifth  = -M_u(b).*conj(v_prime(b));
        sixth  = -(b-x0).*u(b).*conj(v(b));
        seventh = (b-x0).*u_prime(b).*conj(v_prime(b));
    else 
        fourth = 0;
        fifth  = 0;
        sixth  = 0;
        seventh = 0;
    end
       
    %fourth = -1i*k*(conj(M_v(b)).*u(b) + conj(M_v(a)).*u(a));
    %fifth = -M_u(b).*conj(v_prime(b)) + M_u(a).*conj(v_prime(a));
    %sixth =  - (b-x0).*u(b).*conj(v(b)) + (a-x0).*u(a).*conj(v(a));
    %seventh = + (b-x0).*u_prime(b).*conj(v_prime(b)) - (a-x0).*u_prime(a).*conj(v_prime(a));

    res = first + (k^2)*second + third + fourth + fifth + (k^2)*sixth + seventh;
    return
end
 
% mid-intervals
if (bnode_1 == bnode_2) &&  (bnode_1 > 1) && (bnode_1 < N)
    x_pre = table_nodes(bnode_1 - 1);
    x_mid = table_nodes(bnode_1);
    x_post = table_nodes(bnode_1 + 1);
    
    % define basis functions
    if mod(idx1, 2) == 1
        % Left
        uL = @(x) basis(x, x_pre, x_mid, 3);
        uL_prime = @(x) basis_prime(x, x_pre, x_mid, 3);
        uL_prime_prime = @(x) basis_prime_prime(x, x_pre, x_mid, 3);
        % Right
        uR = @(x) basis(x, x_mid, x_post, 1);
        uR_prime = @(x) basis_prime(x, x_mid, x_post, 1);
        uR_prime_prime = @(x) basis_prime_prime(x, x_mid, x_post, 1);
    else   
        % Left 
        uL = @(x) basis(x, x_pre, x_mid, 4);
        uL_prime = @(x) basis_prime(x, x_pre, x_mid, 4);
        uL_prime_prime = @(x) basis_prime_prime(x, x_pre, x_mid, 4);
        % Right
        uR = @(x) basis(x, x_mid, x_post, 2);
        uR_prime = @(x) basis_prime(x, x_mid, x_post, 2);
        uR_prime_prime = @(x) basis_prime_prime(x, x_mid, x_post, 2);
    end
    
    if mod(idx2, 2) == 1
        % Left
        vL = @(x) basis(x, x_pre, x_mid, 3);
        vL_prime = @(x) basis_prime(x, x_pre, x_mid, 3);
        vL_prime_prime = @(x) basis_prime_prime(x, x_pre, x_mid, 3);
        % Right
        vR = @(x) basis(x, x_mid, x_post, 1);
        vR_prime = @(x) basis_prime(x, x_mid, x_post, 1);
        vR_prime_prime = @(x) basis_prime_prime(x, x_mid, x_post, 1);
    else   
        % Left 
        vL = @(x) basis(x, x_pre, x_mid, 4);
        vL_prime = @(x) basis_prime(x, x_pre, x_mid, 4);
        vL_prime_prime = @(x) basis_prime_prime(x, x_pre, x_mid, 4);
        % Right
        vR = @(x) basis(x, x_mid, x_post, 2);
        vR_prime = @(x) basis_prime(x, x_mid, x_post, 2);
        vR_prime_prime = @(x) basis_prime_prime(x, x_mid, x_post, 2);
    end
        
       
    % Morawetz and Helmholtz Operators
    M_uL = @(x) (x-x0).*uL_prime(x)-1i*k*beta*uL(x);
    M_vL = @(x) (x-x0).*vL_prime(x)-1i*k*beta*vL(x);
    L_uL = @(x) uL_prime_prime(x) + k^2 * uL(x);
    L_vL = @(x) vL_prime_prime(x) + k^2 * vL(x);
    
    M_uR = @(x) (x-x0).*uR_prime(x)-1i*k*beta*uR(x);
    M_vR = @(x) (x-x0).*vR_prime(x)-1i*k*beta*vR(x);
    L_uR = @(x) uR_prime_prime(x) + k^2 * uR(x);
    L_vR = @(x) vR_prime_prime(x) + k^2 * vR(x);
    
    % Computation
    lowerbound = x_pre;
    midbound = x_mid;
    upperbound = x_post;

    first_integrandL = @(x) uL_prime(x).*conj(vL_prime(x));
    firstL = integral(first_integrandL,lowerbound,midbound,'ArrayValued', true);
    first_integrandR = @(x) uR_prime(x).*conj(vR_prime(x));
    firstR = integral(first_integrandR,midbound,upperbound,'ArrayValued', true);

    second_integrandL = @(x) uL(x).*conj(vL(x));
    secondL = integral(second_integrandL,lowerbound,midbound,'ArrayValued',true);
    second_integrandR = @(x) uR(x).*conj(vR(x));
    secondR = integral(second_integrandR,midbound,upperbound,'ArrayValued',true);

    third_integrandL = @(x) (M_uL(x) + (A*L_uL(x)/k^2)).*conj(L_vL(x));
    thirdL = integral(third_integrandL,lowerbound,midbound,'ArrayValued',true);
    third_integrandR = @(x) (M_uR(x) + (A*L_uR(x)/k^2)).*conj(L_vR(x));
    thirdR = integral(third_integrandR,midbound,upperbound,'ArrayValued',true);
    
    % Since we consider intervals centered on nodes 2 to N-1,
    % terms fourth to seventh are zero.
          
    res = firstL + firstR + (k^2)*(secondL + secondR) + thirdL + thirdR;
    return

end
       
    
end
        
        
        
    
    




