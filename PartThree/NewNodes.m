function [res] = NewNodes(table_nodes,R,coefs,a,b,k,x0,beta,A_bl,f)
% Returns best node location to add, via OGA. 
% INPUT:  
%         table_nodes,  vector of size n+1,
%         R,            Proportion of nodes to add
%         a,            left-end point,
%         b,            right-end point,
%         k,            frequency,
%         x0,           parameter of bilinear form,
%         beta,         parameter of bilinear form,
%         A,            parameter of bilinear form,
% OUTPUT: index of node left and right of new node


N = length(table_nodes);
N_new = ceil(R*N);
scores = zeros(N-1,1);
new_nodes = zeros(N+N_new,1);

% Calculate score |a(u_N,g)-l(g)|, for each new basis function g
%   u_N is the previous approximation
for i=1:N-1
    scores(i) = OGA_score(i,table_nodes,coefs,a,b,k,x0,beta,A_bl,f);     
end

% Select top N_new nodes
[~, indices] = maxk(scores,N_new);

% Construct new table of nodes
d = 0;
for i=1:N
    if ismember(i,indices)
        new_nodes(i+d) = table_nodes(i);
        new_nodes(i+d+1) = (table_nodes(i) + table_nodes(i+1))/2;
        d = d+1;
    else
        new_nodes(i+d) = table_nodes(i);
    end
end

res = new_nodes;

end