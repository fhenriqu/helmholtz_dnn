function [res] = sesform(table_nodes,a,b,k,x0,beta,A)
% Returns best node location to add, via OGA. 
% INPUT:  
%         idx1,2        index of basis function (1,2*N)
%         table_nodes,  vector of size n+1,
%         a,            left-end point,
%         b,            right-end point,
%         k,            frequency,
%         x0,           parameter of bilinear form,
%         beta,         parameter of bilinear form,
%         A,            parameter of bilinear form,
% OUTPUT: index of node left and right of new node


N = length(table_nodes);

% table that stores <g, u_n -u>, for different nodes
inner_prod = zeros(N-1,1);

% iterate over different possibilities of new node
for i=1:N-1
    
    % Construct new_table of nodes
    new_idx = i+1;
    new_node = (table_nodes(i) + table_nodes(i+1))/2;
    new_table = [table_nodes(1:new_idx-1) new_node table_nodes(new_idx:end)];
      
    %%% FEM to find coeficients of new approximation  %%% 
    
    % Stiffness matrix  
    A = zeros(2*(N+1));
    for idx1=1:2*(N+1)
        for idx2=1:2*(N+1)
            if abs(ceil(idx1/2) - ceil(idx2/2)) < 2
                A(idx2,idx1) = sesform(idx1,idx2,new_table,a,b,k,x0,beta,A_bl);
            end
        end
    end 
    A = sparse(A);
     
    % Right-hand side: l(phi_i)
    l_vec = zeros(2*N,1);
    for k=1:2*(N+1)
        l_vec(k) = rhs(k,new_nodes,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);
    end

    % Solve for coefficients in A*coefs=l_vec
    coefs = A\l_vec;
    
    % Calculate inner product
    
    inner_prod(i) = 0;
    
    
    % find node index that maximises |a(g, u_(n-1)) - l(g)|
    % u_(n)
    

  
    
end
        
        
        
    
    




