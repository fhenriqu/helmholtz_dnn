%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          Implementation of OGA FEM for the Helmholtz Problem          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
clc
close all

% Starting parameters
a    = -1;       % Left-end point        
b    =  1;       % Right-end point
x0   =  (a+b)/2; % Parameter bilinear form
beta = 100;%(b-a)/2;  % Parameter bilinear form
A_bl = 1/3;      % Parameter bilinear form
k    = 10;        % Frequency
N = 15;          % N: number of nodes, 2N: number of basis fcts
R = 0.3;        % Refinement percentage

% Input data
f = @(x)  1 ; 
g_plus  = 0;
g_minus = 0;


% N equidistant nodes
table_nodes = zeros(N,1);
for p=1:N
    table_nodes(p) = a + (p-1)*(b-a)/(N-1);
end

table_nodes
coefs = femRunner(table_nodes,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%   COMPARISON    %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% compare_nodes = zeros(ceil(N*(1+R)^2),1);
% for p=1:ceil(N*(1+R)^2)
%     compare_nodes(p) = a + (p-1)*(b-a)/(N-1);
% end
% 
% compare_coefs = femRunner(compare_nodes);

%%% Refined meshes %%%
% New nodes 1
new_nodes1 = NewNodes(table_nodes,R,coefs,a,b,k,x0,beta,A_bl,f)
new_coefs1 = femRunner(new_nodes1,a,b,k,x0,beta,A_bl,f,g_plus,g_minus)

% New nodes 2
new_nodes2 = NewNodes(new_nodes1,R,new_coefs1,a,b,k,x0,beta,A_bl,f);
new_coefs2 = femRunner(new_nodes2,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);

% Define true solution u(x) for f=1:
u_g = @(x) (1/(2*k^2))*(-2 + exp(1i*k*(x+1)) + exp(-1i*k*(x-1)));
w = @(x) (-exp(1i*k)/(2i*k)).*(g_minus*exp(1i*k*x) + g_plus*exp(-1i*k*x));
u = @(x) u_g(x) + w(x);
u_prime = @(x) (-exp(1i*k)/2)*(g_minus*exp(-1i*k*x) -g_plus*exp(-1i*k)) + ...
                (1i*k^3)/2*(exp(1i*k*(x+1)) - exp(-1i*k*(x-1)));


% Plot approximations and true solution
x = linspace(a,b,300);
y = zeros(length(x), 1);
y1 = zeros(length(x), 1);
y2 = zeros(length(x), 1);
%y3 = zeros(length(x), 1);

figure()
hold on
for ix = 1 : length(x)
    y(ix) = real(approx_sol(x(ix), coefs, table_nodes));
    y1(ix) = real(approx_sol(x(ix), new_coefs1, new_nodes1));
    y2(ix) = real(approx_sol(x(ix), new_coefs2, new_nodes2));
    %y3(ix) = real(approx_sol(x(ix), compare_coefs, compare_nodes));
end
plot(x,y,'DisplayName', "Approx. N ="+N)
plot(x,y1,'DisplayName', "Refined: N ="+ceil(N*(1+R)))
plot(x,y2,'DisplayName', "Refined: N ="+ceil(N*(1+R)^2))
%plot(x,y3,'--', 'DisplayName', "Equidistant: N ="+ceil(N*(1+R)^2))
plot(x, real(u(x)),'b', 'DisplayName', 'Target function');
title('Cubic FEM with Refined Meshes')
subtitle("f(x)=1,(" + a+","+b+"), k="+k+", refinement: "+ 100*R+" %")
legend show
axis padded
grid on






