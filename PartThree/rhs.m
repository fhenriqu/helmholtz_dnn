function [res] = rhs(index,table_nodes,a,b,k,x0,beta,A,f,g_plus,g_minus)
% INPUT:  
%         index,         Number of the basis function {1,...,2N}
%         table_nodes,   Array of nodes 1 to N
%         a,             Left-end point,
%         b,             Right-end point,
%         k,             Frequency,
%         x0,            Parameter of bilinear form,
%         beta,          Parameter of bilinear form,
%         A,          Parameter of bilinear form,
%         f,             Function handler for source term,
%         g_plus,        Impedance boundary condition at x=a,
%         g_minus,       Impedance boundary condition at x=b,

% Number of nodes: N, number of phi-basis fcts: 2N
N = length(table_nodes);


%%% First two basis functions
if (index == 1) || (index == 2)
    x_pre = table_nodes(1);
    x_post = table_nodes(2);
    
    % Construction of the basis functions
    if (index == 1)
        v = @(x) basis(x, x_pre, x_post, 1);
        v_prime = @(x) basis_prime(x, x_pre, x_post, 1);
        v_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 1);
    elseif (index == 2)
        v = @(x) basis(x, x_pre, x_post, 2);
        v_prime = @(x) basis_prime(x, x_pre, x_post, 2);
        v_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 2);
    end
    
    % Morawetz and Helmholtz Operators
    M_v = @(x) (x-x0).*v_prime(x)-1i*k*beta*v(x);
    L_v = @(x) v_prime_prime(x) + k^2 * v(x);
    
    % First contribution
    firstintl = @(x) (conj(M_v(x))-A*conj(L_v(x))/(k^2)).*f(x);
    firstl  = integral(firstintl,x_pre,x_post,'ArrayValued', true);

    % Second contribution
    secondl = 0;

    % Third contribution
    if (index == 1)
        thirdl = conj(-1i*k*beta).*g_minus;
    elseif (index == 2)
        thirdl = (a-x0).*g_minus;
    end

    res = firstl + secondl + thirdl;
    return
    

%%% Last two basis functions
elseif (index == 2*N) || (index == 2*N-1)
    x_pre = table_nodes(N-1);
    x_post = table_nodes(N);
    
    % Construction of the basis functions
    if (index == 2*N-1)
        v = @(x) basis(x, x_pre, x_post, 3);
        v_prime = @(x) basis_prime(x, x_pre, x_post, 3);
        v_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 3);
    elseif (index == 2*N)
        v = @(x) basis(x, x_pre, x_post, 4);
        v_prime = @(x) basis_prime(x, x_pre, x_post, 4);
        v_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 4);
    end
    
    % Morawetz and Helmholtz Operators
    M_v = @(x) (x-x0).*v_prime(x)-1i*k*beta*v(x);
    L_v = @(x) v_prime_prime(x) + k^2 * v(x);
    
    % First contribution
    firstintl = @(x) (conj(M_v(x))-A*conj(L_v(x))/(k^2)).*f(x);
    firstl  = integral(firstintl,x_pre,x_post,'ArrayValued', true);

    % Second contribution
    if (index == 2*N-1)
        secondl = conj(-1i*k*beta).*g_plus;
    elseif (index == 2*N)
        secondl = (b-x0).*g_plus;
    end

    % Third contribution
    thirdl = 0;

    res = firstl + secondl + thirdl;
    return


%%% intermediate basis functions
else 
    interval_idx = ceil(index/2);
    x_pre = table_nodes(interval_idx - 1);
    x_mid = table_nodes(interval_idx);
    x_post = table_nodes(interval_idx + 1);
       
    % Construction of basis functions
    if mod(index, 2) == 1
        % for left interval (x_pre, x_mid)
        vL = @(x) basis(x, x_pre, x_mid, 3);
        vL_prime = @(x) basis_prime(x, x_pre, x_mid, 3);
        vL_prime_prime = @(x) basis_prime_prime(x, x_pre, x_mid, 3);

        % for right interval (x_mid, x_post)
        vR = @(x) basis(x, x_mid, x_post, 1);
        vR_prime = @(x) basis_prime(x, x_mid, x_post, 1);
        vR_prime_prime = @(x) basis_prime_prime(x, x_mid, x_post, 1);
        
    elseif mod(index, 2) == 0
        % for left interval (x_pre, x_mid)
        vL = @(x) basis(x, x_pre, x_mid, 4);
        vL_prime = @(x) basis_prime(x, x_pre, x_mid, 4);
        vL_prime_prime = @(x) basis_prime_prime(x, x_pre, x_post, 4);
        
        % for right interval (x_mid, x_post)
        vR = @(x) basis(x, x_mid, x_post, 2);
        vR_prime = @(x) basis_prime(x, x_mid, x_post, 2);
        vR_prime_prime = @(x) basis_prime_prime(x, x_mid, x_post, 2);
    end    
   
    % Morawetz and Helmholtz Operators
    M_vL = @(x) (x-x0).*vL_prime(x)-1i*k*beta*vL(x);
    M_vR = @(x) (x-x0).*vR_prime(x)-1i*k*beta*vR(x);
    L_vL = @(x) vL_prime_prime(x) + k^2 * vL(x);
    L_vR = @(x) vR_prime_prime(x) + k^2 * vR(x);
    
    
    %%%% LEFT INTERVAL %%%
    
    % First contribution
    firstintL = @(x) (conj(M_vL(x))-A*conj(L_vL(x))/(k^2)).*f(x);
    firstL  = integral(firstintL,x_pre,x_mid,'ArrayValued', true);

    % Second contribution
    secondL = 0;

    % Third contribution
    thirdL = 0;
    
    %%% RIGHT INTERVAL %%%
    
     % First contribution
    firstintR = @(x) (conj(M_vR(x))-A*conj(L_vR(x))/(k^2)).*f(x);
    firstR  = integral(firstintR,x_mid, x_post,'ArrayValued', true);
    
    % Second contribution
    secondR = 0;

    % Third contribution
    thirdR = 0;
    
    res = firstL + secondL + thirdL + firstR + secondR + thirdR;
    return

end
