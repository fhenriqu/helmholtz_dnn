function [res] = OGA_score(node_idx,table_nodes,coefs,a,b,k,x0,beta,A,f)
% INPUT:  
%         node_idx      index of node left of inserted node
%         table_nodes,  vector of size n+1,
%         a,            left-end point,
%         b,            right-end point,
%         k,            frequency,
%         x0,           parameter of bilinear form,
%         beta,         parameter of bilinear form,
%         A,            parameter of bilinear form,
% OUTPUT: Bilinear form a(u,v), u=phi_idx1, v=phi_idx2

x_pre = table_nodes(node_idx);
x_post = table_nodes(node_idx+1);
x_mid = (x_pre + x_post)/2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%      Calculating a(u_N,g)    %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Left interval: (x_pre, x_mid)
part11L = sesformOGA(x_pre,x_post,1,x_pre,x_mid,3,2*node_idx-1,coefs,a,b,k,x0,beta,A);
part21L = sesformOGA(x_pre,x_post,2,x_pre,x_mid,3,2*node_idx,coefs,a,b,k,x0,beta,A);
part12L = sesformOGA(x_pre,x_post,1,x_pre,x_mid,4,2*node_idx-1,coefs,a,b,k,x0,beta,A);
part22L = sesformOGA(x_pre,x_post,2,x_pre,x_mid,4,2*node_idx-1,coefs,a,b,k,x0,beta,A);

% Right interval: (x_mid, x_post)
part11R = sesformOGA(x_pre,x_post,3,x_mid,x_post,1,2*node_idx+1,coefs,a,b,k,x0,beta,A);
part21R = sesformOGA(x_pre,x_post,4,x_mid,x_post,2,2*node_idx+2,coefs,a,b,k,x0,beta,A);
part12R = sesformOGA(x_pre,x_post,3,x_mid,x_post,1,2*node_idx+1,coefs,a,b,k,x0,beta,A);
part22R = sesformOGA(x_pre,x_post,4,x_mid,x_post,2,2*node_idx+2,coefs,a,b,k,x0,beta,A);

a_u_g = part11L + part21L + part12L + part22L ...
        + part11R + part21R + part12R + part22R;
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%      Calculating l(g)        %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

l_g = -rhsOGA(x_pre,x_mid,x_post,k,x0,beta,A,f);

res = abs(a_u_g - l_g);

end
        
    
    




