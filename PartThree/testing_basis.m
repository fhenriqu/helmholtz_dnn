close all 
clear 
clc

% Interval Bounds
x_i = 0;
x_post = 100;
x = linspace(x_i, x_post);

% Theta functions
theta1 = basis(x, x_i, x_post, 1);
theta2 = basis(x, x_i, x_post, 2);
theta3 = basis(x, x_i, x_post, 3);
theta4 = basis(x, x_i, x_post, 4);

f = @(x) x - x_i;
g = @(x) x - x_post; 

% Plotting
set(groot,'defaultLineLineWidth',1.0)

figure
hold on
plot(x,theta1,'DisplayName', 'theta1')
plot(x,theta2,'DisplayName', 'theta2')
plot(x,theta3,'DisplayName', 'theta3')
plot(x,theta4,'DisplayName', 'theta4')
plot(x,f(x), '--','DisplayName', 'devcheckpre')
plot(x,g(x), '--','DisplayName', 'devcheckpost')
legend show
axis padded
grid on



% Derivatives
theta1Prime = basis_prime(x, x_i, x_post, 1);
theta2Prime = basis_prime(x, x_i, x_post, 2);
theta3Prime = basis_prime(x, x_i, x_post, 3);
theta4Prime = basis_prime(x, x_i, x_post, 4);

% Plotting
figure
hold on

plot(x,theta1Prime, '--','DisplayName', 'theta1Prime')
plot(x,theta2Prime, '--','DisplayName', 'theta2Prime')
plot(x,theta3Prime, '--','DisplayName', 'theta3Prime')
plot(x,theta4Prime, '--','DisplayName', 'theta4Prime')

legend show
axis padded
grid on



% Second order derivatives
theta1PrimePrime = basis_prime_prime(x, x_i, x_post, 1);
theta2PrimePrime = basis_prime_prime(x, x_i, x_post, 2);
theta3PrimePrime = basis_prime_prime(x, x_i, x_post, 3);
theta4PrimePrime = basis_prime_prime(x, x_i, x_post, 4);

% Plotting
figure
hold on

plot(x,theta1PrimePrime,'DisplayName', 'theta1PrimePrime')
plot(x,theta2PrimePrime, '--','DisplayName', 'theta2PrimePrime')
plot(x,theta3PrimePrime, '--','DisplayName', 'theta3PrimePrime')
plot(x,theta4PrimePrime, '--','DisplayName', 'theta4PrimePrime')

legend show
axis padded
grid on




