function [out] = basis(x, x_i, x_post, k)
% INPUT:  x,      value to evaluate 
%         x_i,    first node of interval I_i
%         x_post, last node of interval
%         k,      index of hermite basis polynomial: theta_j
% OUTPUT: value of hermite basis polynomial scaled to element I_i

h = x_post - x_i;
t = (x - x_i)./h;

if k == 1
    out = (2*t+1) .* (t-1).^2;
elseif k == 2
    out = h*t .* (t-1).^2;
elseif k == 3
    out = (3-2*t).* (t.^2);
elseif k == 4
    out = h*(t-1) .* (t.^2);
end

