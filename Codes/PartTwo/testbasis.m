clear all
close all

% Parameters
a = -1;
b =  1;

% Construction of the basis functions
n = 5;
c_min  = -0.5 ;
c_max  =  0.5 ;
params = zeros(2*n+2,2);
kp  = 3;
for p=1:n+1
    % +1
    params(p,1)     = +1;  
    params(p,2)     = c_min+(p-1)/n*(c_max-c_min);
    % -1
    params(p+n+1,1) = -1;
    params(p+n+1,2) = c_min+(p-1)/n*(c_max-c_min);
end

Ind = params(:,2) + max([a*params(:,1)';b*params(:,1)'])'>0

figure(1)
hold on
x = linspace(a,b,100);
i=3;
plot(x,ReLU(x,params(i,1),params(i,2),kp),'DisplayName', 'Target');
x = a:(b-a)/100:b
for ix = 1 : length(x)
    y1(ix) = ReLUPrime(x(ix),params(i,1),params(i,2),kp);
    y2(ix) = ReLUPrimePrime(x(ix),params(i,1),params(i,2),kp);
end
    plot(x,y1,'.-','DisplayName', 'dev');
    plot(x,y2,'DisplayName', 'devdev');
    
hold off
%title('')
%subtitle('u(x)=sin(pi\dotx), interval:(0,1), k=10, n=32')
legend show
axis padded
grid on
