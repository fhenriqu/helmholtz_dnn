clear all
clc
close all

% Starting parameters
a = -1;
x_min = a;
b = 1;
x_max = b;
x0 = 0.5; % x0 inside (a,b)
beta = 1;
Alpha = 2;
k = 3;
n=15;

% define f = -u'' -k^2*u 
f = @(x) 1;
g_plus = 1;
g_minus = -1;


params = zeros(2*n,2);
for p=1:2*n
    if p <= n
        params(p,1) = 1;  
        params(p,2) = -1+2*p/n;
    else
        params(p,1) = -1;
        params(p,2) = -1+2*(2*n+1-p)/n;
    end
end
params 

% Define (A)ij = a(phi_i,phi_j)
A = zeros(2*n);
for i=1:2*n
    for j=1:2*n
        A(i,j) = bilinearform(params(i,1),params(i,2),2,params(j,1),params(j,2),2);
    end
end
A

% calculate b in A*c = b, b_i = l(phi_i)
b = zeros(2*n,1);
for i=1:2*n
    b(i) = l(params(i,1),params(i,2),2);
end
b



% solve for c in A*c=b
c = A\b








function [res] = bilinearsigmoid(w1,b1,kappa1,w2,b2,kappa2)

a = -1;
b = 1;
x0 = 0; % x0 inside (a,b)
beta = 1;
A = 2;
k = 3;

% define basis functions
u = @(x) 1/(1 + exp(-(w1.*x +b1)));
u_prime = @(x) exp(-(w1.*x +b1)/((1 + exp(-(w1.*x +b1))).^2);
u_prime_prime = @(x) (-exp(w1.*x +b1) + exp(-2*(w1.*x +b1)))./((1+exp(-(w1.*x +b1)).^3));

u = @(x) 1/(1 + exp(-(w2.*x +b2)));
u_prime = @(x) exp(-(w2.*x +b2)/((1 + exp(-(w2.*x +b2))).^2);
u_prime_prime = @(x) (-exp(w2.*x +b2) + exp(-2*(w2.*x +b2)))./((1+exp(-(w2.*x +b2)).^3));

% defining formulas 
M_u = @(x) (x-x0).*u_prime(x)-1i*k*beta*u(x);
M_v = @(x) (x-x0).*v_prime(x)-1i*k*beta*v(x);
L_u = @(x) u_prime_prime(x) + k^2 * u(x);
L_v = @(x) v_prime_prime(x) + k^2 * v(x);
 
% calculation of a(u,v) = result
first_integrand = @(x) u_prime(x).*conj(v_prime(x));
first = integral(first_integrand,a,b,'ArrayValued', true);
second_integrand = @(x) u(x).*conj(v(x));
second = k^2*integral(second_integrand,a,b,'ArrayValued',true);
third_integrand = @(x) (M_u(x) + (A*L_u(x)/k^2)).*conj(L_v(x));
third = integral(third_integrand,a,b, 'ArrayValued',true);
fourth = -1i*k*(conj(M_v(b)).*u(b) + conj(M_v(a)).*u(a));
fifth = -M_u(b).*conj(v_prime(b)) + M_u(a).*conj(v_prime(a));
sixth =  - (b-x0).*(k^2).*u(b).*conj(v(b)) + (a-x0).*(k^2).*u(a).*conj(v(a));
seventh = + (b-x0).*u_prime(b).*conj(v_prime(b)) - (a-x0).*u_prime(a).*conj(v_prime(a));
res = first + second + third + fourth + fifth + sixth + seventh;

