function [res] = l(w,nu,kappa,a,b,k,x0,beta,A,f,g_plus,g_minus)
% INPUT:  w,       Paramter of the basis function,
%         nu,      Paramter of the basis function,
%         kappa,   Paramter of the basis function,
%         a,       Left-end point,
%         b,       Right-end point,
%         k,       Frequency,
%         x0,      Parameter of bilinear form
%         beta,    Parameter of bilinear form
%         A,       Parameter of bilinear form
%         f,       Function handler for source term,
%         g_plus,  Impedance boundary condition at x=a,
%         g_minus, Impedance boundary condition at x=b,

% Construction of the basis functions
v = @(x) ReLU(x,w,nu,kappa);
v_prime = @(x) ReLUPrime(x,w,nu,kappa);
v_prime_prime = @(x) ReLUPrimePrime(x,w,nu,kappa);

% Morawetz and Helmholtz Operators
M_v = @(x) (x-x0).*v_prime(x)-1i*k*beta*v(x);
L_v = @(x) v_prime_prime(x) + k^2 * v(x);


%%%%%%% integration bounds for terms 1 to 3 %%%%%%%%%%%%%
lowerbound=a;
upperbound=b;
zero=-nu/w;
if (w<0 && zero<a) || (w>0 && zero>b)
    res=0;
    return
elseif w>0
    lowerbound = max(a,zero);
    upperbound = b;
elseif w<0
    lowerbound = a;
    upperbound = min(zero,b);
end

% First contribution
firstintl = @(x) (conj(M_v(x))-A*conj(L_v(x))/(k^2)).*f(x);
firstl  = integral(firstintl,lowerbound,upperbound,'ArrayValued', true);

% Second contribution
secondl = conj(M_v(b)).*g_plus;

% Third contribution
thirdl  = conj(M_v(a)).*g_minus;

res = firstl + secondl + thirdl;
