function [res] = subject(params,kp,coefs,index,w2,b2,a,b,k,x0,beta,A_bl,f,g_plus,g_minus)
% INPUT:  params,  Paramters of existing basis functions,
%         kp,      Paramter of all basis functions,
%         w2,      Parameter of next basis function,
%         b2,      Parameter of next basis function,
%         a,       Left-end point,
%         b,       Right-end point,
%         k,       Frequency,
%         x0,      Parameter of bilinear form,
%         beta,    Parameter of bilinear form,
%         A_bl,       Parameter of bilinear form,
%         f,       Function handler for source term,
%         g_plus,  Impedance boundary condition at x=a,
%         g_minus, Impedance boundary condition at x=b,
% OUTPUT: -|<g, u_(n-1) - u >| = - |a(g,u_(n-1)) - l(g)| %%% from OGA

% calculation of a(g,u_(n-1)), where g(x) = ReLU(x,w2,b2,kp),
temp = 0;
for i=1:index
    w     = [params(i,1), w2] ;
    nu    = [params(i,2), b2] ;
    kappa = [kp         , kp] ;
    temp = temp + coefs(i)*bilinearform(w,nu,kappa,a,b,k,x0,beta,A_bl);
end

g = @(x) ReLU(x,w2,b2,kp);
gprime = @(x) ReLUPrime(x,w2,b2,kp);
gprimeprime = @(x) ReLUPrimePrime(x,w2,b2,kp);

lowerbound=a;
upperbound=b;
zero = -b2/w2;

active = true; 
if (w2<0 && zero<a) || (w2>0 && zero>b)
    active = false;

elseif w2>0
    lowerbound = max(a,zero);
    upperbound = b;
elseif w2<0
    lowerbound = a;
    upperbound = min(b,zero);
end
        
if active == true
    first_int = @(x) g(x).*conj(g(x));
    first = integral(first_int, lowerbound, upperbound,'ArrayValued',true);
    second_int = @(x) gprime(x).*conj(gprime(x));
    second = integral(second_int,lowerbound,upperbound,'ArrayValued',true);
    third_int = @(x) gprimeprime(x).*conj(gprimeprime(x));
    third = k^(-2) * integral(third_int,lowerbound,upperbound,'ArrayValued',true);
    fourth = k^2 * (b-a)*( (g(a)*conj(g(a)) + g(b)*conj(g(b))));
    fifth = (b-a)*(gprime(a)*conj(gprime(a)) + gprime(b)*conj(gprime(b)));
    
    normalizer = sqrt(first + second + third + fourth + fifth); 
else 
    normalizer = eps;
end

res = -abs( temp - l(w2,b2,kp,a,b,k,x0,beta,A_bl,f,g_plus,g_minus))/normalizer;
