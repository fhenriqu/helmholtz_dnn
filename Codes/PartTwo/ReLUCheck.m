clear all
clc

% ReLUk Functions check: 
w=-1;
b=2;
kappa = 2;

phi = @(x) ReLU(x,w,b,kappa);
phi_prime = @(x) ReLUPrime(x,w,b,kappa);

% Plot
x = linspace(-5,5);
figure
hold on
plot(x,phi(x),'DisplayName', 'phi')

plot(x,phi_prime(x))%,'DisplayName','phi_prime')
axis padded
grid on
%legend show

x = -10: .02 : 10;
for ix = 1 : length(x)
    y(ix) = phi_prime(x(ix));
end
plot(x, y,'DisplayName','phi prime')

hold off


%phi1 = @(x) phi(x,w1,b1,kappa);
%phi1_prime = @(x) phi_prime(x,w1,b1,kappa);



% 
% function v = phi(x,w,b,kappa)
%     v = (max(0,w*x+b)).^kappa;
% end
% 
% function v = phi_prime(x,w,b,kappa)
%  if w == 0
%      v = 0;
%  elseif w > 0
%      if x > -b/w
%          v = kappa*w*(w*x+b).^(kappa-1);
%      else
%          v = 0;
%      end
%  else %w < 0
%      if x < -b/w
%          v = kappa*w*(w*x+b).^(kappa-1);
%      else
%          v = 0;
%      end
%  end
% end
