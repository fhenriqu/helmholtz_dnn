clear all
close all
clc

% Starting parameters
a    = -1;       % Left-end point        
b    =  1;       % Right-end point
x0   =  (a+b)/2; % Parameter bilinear form
beta = 100;%(b-a)/2;  % Parameter bilinear form
A_bl = 100;      % Parameter bilinear form
k    = 1;       % Frequency


% Input data
f = @(x)  0 ;
g_plus  = 1;
g_minus = 1;
kp = 3;
n = 60;

params = zeros(n,2);
coefs = zeros(n,1);

Astored = zeros(n);

for i=1:n
    index = i;
    % find best parameters w,b for basis function i+1
    [params(i,1),params(i,2)] = nextparams(params,kp,coefs,index,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);
    params
    
    % Update coefs
    % Construction of the stiffness matrix: Define (A)ij = a(phi_i,phi_j)
    A = Astored(1:i,1:i);
    for j=1:i
        w     = [params(i,1), params(j,1)] ;
        nu    = [params(i,2), params(j,2)] ;
        kappa = [kp         , kp         ] ;
        A(i,j) = bilinearform(w,nu,kappa,a,b,k,x0,beta,A_bl);
        w     = [params(j,1), params(i,1)] ;
        nu    = [params(j,2), params(i,2)] ;
        kappa = [kp         , kp         ] ;
        A(j,i) = bilinearform(w,nu,kappa,a,b,k,x0,beta,A_bl);
    end
    A;

    Astored;
    Astored(i,1:i) = A(i,:);
    Astored(1:i,i) = A(:,i);
    Astored;
    
%     for p=1:i
%         for j=1:i
%             w     = [params(p,1), params(j,1)] 
%             nu    = [params(p,2), params(j,2)] 
%             kappa = [kp         , kp         ] ;
%             A(i,j) = bilinearform(w,nu,kappa,a,b,k,x0,beta,A_bl);
%         end
%     end
%     AStored = A;
    
    % Construction of the right-hand side: Calculate c in A*x_sol = c, c_i = l(phi_i)
    c = zeros(i,1);
    for p=1:i
        c(p) = l(params(p,1),params(p,2),kp,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);
    end
    c;
 
    % Solve for c in A*coefs=c
    %coefs;
    %coefs_1toi=coefs(1:i);
    %Aoverc = A\c;
    coefs(1:i)=A\c  
end 

% Define true solution u(x) for f=1:
u_g = @(x) (1/(2*k^2))*(-2 + exp(1i*k*(x+1)) + exp(-1i*k*(x-1)));
w = @(x) (-exp(1i*k)/(2i*k)).*(g_minus*exp(1i*k*x) + g_plus*exp(-1i*k*x));
u = @(x) u_g(x) + w(x);

% Active Basis
Ind = params(:,2) + max([a*params(:,1)';b*params(:,1)'])'>0;

% Plot
x = linspace(a,b,200);
figure(1)
hold on
plot(x,real(w_n(x,coefs,n,params,kp,Ind)), 'DisplayName', 'Approx');
plot(x,real(u(x)), 'DisplayName', 'Target');
hold off
title('OGA approx')
subtitle("f(x)=0, interval:(" + a+","+b+"), k="+k+", n="+n)
legend show
axis padded
grid on


function res = w_n(x,c,n,params,kp,Ind)
temp = 0;
for i=1:n
    if Ind(i) == 0
        continue
    end
    zero = -params(i,2)/params(i,1);
    if params(i,1) > 0 % w>0
        if x <= zero
            continue
        end 
    else % w<0
        if x >= zero
            continue
        end
    end    
    temp = temp + c(i)*ReLU(x,params(i,1),params(i,2),kp);
end
res = temp;
end



% Construction of best w,b parameters for next basis function:
function [wres,bres] = nextparams(params,kp,coefs,index,a,b,k,x0,beta,A_bl,f,g_plus,g_minus)
    % find w,b of next basis function g, that maximise |a(g,u_(n-1)) - l(g)|
    w2 = 1;
    fun1 = @(b2) subject(params,kp,coefs,index,w2,b2,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);
 
    [bval1,fval1] = fminbnd(fun1,-2,2);
    w2 = -1;
    fun2 = @(b2) subject(params,kp,coefs,index,w2,b2,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);
    [bval2,fval2] = fminbnd(fun2,-2,2);
    
    if fval1 < fval2
        wres = 1;
        bres = bval1;
    else 
        wres = -1;
        bres = bval2;
    end
end 


% 
% for i=1:n
%     % find best parameters w & b for next basis function:
%     w2 = 1;
%     index = i;
%     fun1 = @(b2) subject(params,kp,coefs,index,w2,b2,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);
%  
%     [bval1,fval1] = fminbnd(fun1,-2,2)
%     w2 = -1;
%     fun2 = @(b2) subject(params,kp,coefs,k,w2,b2,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);
%     [bval2,fval2] = fminbnd(fun2,-2,2)
%     
%     if fval1 < fval2
%         params(i+1,1) = 1;
%         params(i+1,2) = bval1;
%     else 
%         params(i+1,1) = -1;
%         params(i+1,2) = bval2;
%     end
%     params
%     
%     
% % Active Basis
% Ind = params(:,2) + max([a*params(:,1)';b*params(:,1)'])'>0;    
%     
% % Construction of the stiffness matrix: Define (A)ij = a(phi_i,phi_j)
% A = zeros(k+1);
% for i=1:k+1
%     for j=1:k+1
%         w     = [params(i,1), params(j,1)] ;
%         nu    = [params(i,2), params(j,2)] ;
%         kappa = [kp         , kp         ] ;
%         A(i,j) = bilinearform(w,nu,kappa,a,b,k,x0,beta,A_bl);
%     end
% end
% 
% 
% 
% 
% % Construction of the right-hand side: Calculate c in A*x_sol = c, c_i = l(phi_i)
% c = zeros(k+1,1);
% for i=1:k+1
%     c(i) = l(params(i,1),params(i,2),kp,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);
% end
% 
% 
% % Solve for c in A*c=b
% x_sol = zeros(2*n+2,1);
% x_sol(Ind) = A(Ind,Ind)\c(Ind) ;
% 
% 
% 
% 




% 
%     
%     % Define (A)ij = a(phi_i,phi_j)
%     A = zeros(k+1);
%     for i=1:k+1
%         for j=1:k+1
%             A(i,j) = bilinearform(params(i,1),params(i,2),2,params(j,1),params(j,2),2);
%         end
%     end
%     A
%     
%     % define (b)i = l(g_i)
%     b = zeros(k+1,1);
%     for i=1:k+1
%         b(i) = l(params(i,1),params(i,2),2);
%     end
%     b
%     % solve for coefs:
%     coefs = A\b
%  
%     % approx = @(x) w_n(x,coefs,params);
%     % approx_prime = @(x) w_n_prime(x,coefs,params);
%     % plot? 
%     % calculate error?
%     % estimate constant?
% end
% params
% 
% 
% 
% 
% 
% function res = w_n(x,c,n,params)
% temp = 0;
% for i=1:2*n
%    temp = temp + c(i).*ReLU(x,params(i,1),params(i,2),2);     
% end
% res = temp;
% end
% 
% function res = w_n_prime(x,c,n,params)
% temp = 0;
% for i=1:2*n
%    temp = temp + c(i).*ReLUPrime(x,params(i,1),params(i,2),2);     
% end
% res = temp;
% end
 
% 
% % function res = approx(x,c,params)
% % temp = 0;
% % for i=1:n
% %    if params(i,1)>0 && x < -b_i/w_i
% %     continue
% %    end
% %    if params(i,1)<0 && x > -b_i/w_i
% %        continue
% %    end       
% %    temp = temp + c(i).*ReLU(x,params(i,1),params(i,2),2);     
% % end
% % res = temp;
% % end
% % 
% % function res = approx_prime(x,c,params)
% % temp = 0;
% % for i=1:n
% %    if params(i,1)>0 && x < -b_i/w_i
% %     continue
% %    end
% %    if params(i,1)<0 && x > -b_i/w_i
% %        continue
% %    end       
% %    temp = temp + c(i).*ReLUPrime(x,params(i,1),params(i,2),2);     
% % end
% % res = temp;
% % end
% % 
