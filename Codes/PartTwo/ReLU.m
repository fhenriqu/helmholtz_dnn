function [res] = ReLU(x,w,b,kappa)

res = max(0,(w*x+b)).^kappa;
