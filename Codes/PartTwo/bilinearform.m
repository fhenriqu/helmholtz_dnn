function [res] = bilinearform(w,nu,kappa,a,b,k,x0,beta,A)
% INPUT:  w,     vector of size 2,
%         nu,    vector of size 2,
%         kappa, vectof of size 2,
%         a,     left-end point,
%         b,     right-end point,
%         k,     frequency,
%         x0,    parameter of bilinear form
%         beta,  parameter of bilinear form
%         A,     parameter of bilinear form
% OUTPUT: Bilinear form a(phi_i,phi_j)

% Set parameters of basis functions
w1  = w(1) ; w2  = w(2) ; 
nu1 = nu(1); nu2 = nu(2);
kappa1 = kappa(1) ;
kappa2 = kappa(2) ;

% Define basis functions
u = @(x) ReLU(x,w1,nu1,kappa1);
u_prime = @(x) ReLUPrime(x,w1,nu1,kappa1);
u_prime_prime = @(x) ReLUPrimePrime(x,w1,nu1,kappa1);

v = @(x) ReLU(x,w2,nu2,kappa2);
v_prime = @(x) ReLUPrime(x,w2,nu2,kappa2);
v_prime_prime = @(x) ReLUPrimePrime(x,w2,nu2,kappa2);

% Morawetz and Helmholtz Operators
M_u = @(x) (x-x0).*u_prime(x)-1i*k*beta*u(x);
M_v = @(x) (x-x0).*v_prime(x)-1i*k*beta*v(x);
L_u = @(x) u_prime_prime(x) + k^2 * u(x);
L_v = @(x) v_prime_prime(x) + k^2 * v(x);
 

zero1=-nu1/w1;
zero2=-nu2/w2;


%%%%%%% integration bounds for terms 1 to 3 %%%%%%%%%%%%%
lowerbound=a;
upperbound=b;

if (w1<0 && zero1<a) || (w1>0 && zero1>b) || (w2<0 && zero2<a) || (w2>0 && zero2>b)
    res  = 0;
    return
elseif w1>0
        if w2>0
            lowerbound = max([zero1,zero2,a]);
            upperbound = b;
        elseif w2<0
            if zero2 < zero1
                res=0;
                return
            else
                lowerbound = max([zero1,a]);
                upperbound = min([zero2,b]);
            end
        end     
elseif w1<0
        if w2<0
            lowerbound = a;
            upperbound = min([zero1,zero2,b]);
        elseif w2>0
            if zero1<zero2
                res=0;
                return
            else
                lowerbound = max([zero2,a]);
                upperbound = min([zero1,b]);
            end
        end
end
             
%%%%%%% computation %%%%%%%%%%%%%

first_integrand = @(x) u_prime(x).*conj(v_prime(x));
first = integral(first_integrand,lowerbound,upperbound,'ArrayValued', true);
 
second_integrand = @(x) u(x).*conj(v(x));
second = integral(second_integrand,lowerbound,upperbound,'ArrayValued',true);

third_integrand = @(x) (M_u(x) + (A*L_u(x)/k^2)).*conj(L_v(x));
third = integral(third_integrand,lowerbound,upperbound, 'ArrayValued',true);

fourth = -1i*k*(conj(M_v(b)).*u(b) + conj(M_v(a)).*u(a));

fifth = -M_u(b).*conj(v_prime(b)) + M_u(a).*conj(v_prime(a));

sixth =  - (b-x0).*u(b).*conj(v(b)) + (a-x0).*u(a).*conj(v(a));

seventh = + (b-x0).*u_prime(b).*conj(v_prime(b)) - (a-x0).*u_prime(a).*conj(v_prime(a));

res = first + (k^2)*second + third + fourth + fifth + (k^2)*sixth + seventh;

