function [res] = ReLUPrimePrime(x,w,b,kappa)
 if w == 0
     res = 0;
 elseif w > 0
     if x > -b/w
         res = kappa*(kappa-1)*w^(2)*(w*x+b).^(kappa-2);
     else
         res = 0;
     end
 else % w < 0
     if x < -b/w
         res = kappa*(kappa-1)*w^(2)*(w*x+b).^(kappa-2);
     else
         res = 0;
     end
 end
end
