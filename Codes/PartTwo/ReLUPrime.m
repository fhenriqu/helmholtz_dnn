function [res] = ReLUPrime(x,w,b,kappa)
 if w == 0
     res = 0;
 elseif w > 0
     if x > -b/w
         res = kappa*w*(w*x+b).^(kappa-1);
     else
         res = 0;
     end
 else % w < 0
     if x < -b/w
         res = kappa*w*(w*x+b).^(kappa-1);
     else
         res = 0;
     end
 end
end
