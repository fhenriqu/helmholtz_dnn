%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Implementation of the Finite Element Method for the Helmholtz Problem %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
clc
close all

% Starting parameters
a    = -1;       % Left-end point        
b    =  1;       % Right-end point
x0   =  (a+b)/2; % Parameter bilinear form
beta = 100;%(b-a)/2;  % Parameter bilinear form
A_bl = 1/3;      % Parameter bilinear form
k    = 1;       % Frequency

% Construction of the basis functions
n = 3;
c_min  = -2.5 ;
c_max  =  2.5  ;
params = zeros(2*n+2,2);
kp  = 3;
for p=1:n+1
    % +1
    params(p,1)     = +1;  
    params(p,2)     = c_min+(p-1)/n*(c_max-c_min);
    % -1
    params(p+n+1,1) = -1;
    params(p+n+1,2) = c_min+(p-1)/n*(c_max-c_min);
end

% Active Basis
Ind = params(:,2) + max([a*params(:,1)';b*params(:,1)'])'>0;

% Input data
f = @(x)  0 ;
g_plus  = 1;
g_minus = 1;


% Construction of the stiffness matrix: Define (A)ij = a(phi_i,phi_j)
A = zeros(2*n+2);
for i=1:2*n+2
    for j=1:2*n+2
        w     = [params(i,1), params(j,1)] ;
        nu    = [params(i,2), params(j,2)] ;
        kappa = [kp         , kp         ] ;
        A(i,j) = bilinearform(w,nu,kappa,a,b,k,x0,beta,A_bl);
    end
end

% Construction of the right-hand side: Calculate c in A*x_sol = c, c_i = l(phi_i)
c = zeros(2*n+2,1);
for i=1:2*n+2
    c(i) = l(params(i,1),params(i,2),kp,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);
end


% Solve for c in A*c=b
x_sol = zeros(2*n+2,1);
x_sol(Ind) = A(Ind,Ind)\c(Ind) ;

% Define true solution u(x) for f=1:
u_g = @(x) (1/(2*k^2))*(-2 + exp(1i*k*(x+1)) + exp(-1i*k*(x-1)));
w = @(x) (-exp(1i*k)/(2i*k)).*(g_minus*exp(1i*k*x) + g_plus*exp(-1i*k*x));
u = @(x) u_g(x) + w(x);
% 
u_prime = @(x) (-exp(1i*k)/2)*(g_minus*exp(-1i*k*x) -g_plus*exp(-1i*k))+ (1i*k^3)/2*(exp(1i*k*(x+1)) - exp(-1i*k*(x-1)));


% Plot w_n and u
x = linspace(a,b,200);
 
figure(1)
hold on
plot(x,real(w_n(x,x_sol,n,params,kp,Ind)), 'DisplayName', 'Approx');
plot(x,real(u(x)), 'DisplayName', 'Target');
hold off
%title('')
subtitle("f(x)=1, interval:(" + a+","+b+"), k="+k+", n="+n)
legend show
axis padded
grid on


figure(2)
hold on
plot(x,real(w_n_prime(x,x_sol,n,params,kp,Ind)), 'DisplayName', 'Approx');
plot(x,real(u_prime(x)), 'DisplayName', 'Target derivative');
hold off
%title('')
subtitle("f(x)=1, interval:(" + a+","+b+"), k="+k+", n="+n)
legend show
axis padded
grid on


function res = w_n(x,c,n,params,kp,Ind)
temp = 0;
for i=1:2*n+2
    if Ind(i) == 0
        continue
    end
    zero = -params(i,2)/params(i,1);
    if params(i,1) > 0 % w>0
        if x <= zero
            continue
        end 
    else % w<0
        if x >= zero
            continue
        end
    end    
    temp = temp + c(i)*ReLU(x,params(i,1),params(i,2),kp);
end
res = temp;
end


function res = w_n_prime(x,c,n,params,kp,Ind)
temp = 0;
for i=1:2*n+2
    if Ind(i) == 0
        continue
    end
    zero = -params(i,2)/params(i,1);
    if params(i,1) > 0 % w>0
        if x <= zero
            continue
        end 
    else % w<0
        if x >= zero
            continue
        end
    end    
    temp = temp + c(i)*ReLUPrime(x,params(i,1),params(i,2),kp);
end
res = temp;
end


