%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Implementation of the Finite Element Method for the Helmholtz Problem %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
clc
close all

% Starting parameters
a    = -1;       % Left-end point        
b    =  1;       % Right-end point
x0   =  (a+b)/2; % Parameter bilinear form
beta = (b-a)/2;  % Parameter bilinear form
A_bl = 1/3;      % Parameter bilinear form
k    = 1;       % Frequency

% Construction of the basis functions
n = 1;
c_min  = -2.5 ;
c_max  =  2.5  ;
params = zeros(2*n+2,2);
kp  = 2;
for p=1:n+1
    % +1
    params(p,1)     = +1;  
    params(p,2)     = c_min+(p-1)/n*(c_max-c_min);
    % -1
    params(p+n+1,1) = -1;
    params(p+n+1,2) = c_min+(p-1)/n*(c_max-c_min);
end
params;

% Active Basis
Ind = params(:,2) + max([a*params(:,1)';b*params(:,1)'])'>0;

% Input data
f = @(x) funct(x,params,kp,k,n); 
% g_plus  = 1;
% g_minus = 1;

temp_plus = 0;
temp_minus = 0;
for i=1:2*n+2
    w = params(i,1);
    nu = params(i,2);
    kappa = kp;
    temp_minus = temp_minus - log(i)*ReLUPrime(a,w,nu,kappa) - log(i)*1i*k*ReLU(a,w,nu,kappa);
    temp_plus = temp_plus + log(i)*ReLUPrime(b,w,nu,kappa) - log(i)*1i*k*ReLU(b,w,nu,kappa);
end 
g_minus = temp_minus;
g_plus = temp_plus;

% Construction of the stiffness matrix: Define (A)ij = a(phi_i,phi_j)
A = zeros(2*n+2);
for i=1:2*n+2
    for j=1:2*n+2
        w     = [params(i,1), params(j,1)] ;
        nu    = [params(i,2), params(j,2)] ;
        kappa = [kp         , kp         ] ;
        A(i,j) = bilinearform(w,nu,kappa,a,b,k,x0,beta,A_bl);
    end
end

% Construction of the right-hand side: Calculate c in A*x_sol = c, c_i = l(phi_i)
c = zeros(2*n+2,1);
for i=1:2*n+2
    c(i) = l(params(i,1),params(i,2),kp,a,b,k,x0,beta,A_bl,f,g_plus,g_minus);
end

% Solve for c in A*c=b
x_sol = zeros(2*n+2,1);
x_sol(Ind) = A(Ind,Ind)\c(Ind) 

% coefficients should be: 
coefs = zeros(2*n+2,1);
for i=1:2*n+2
    coefs(i) = log(i);
end

xsolapprox = @(x) w_n(x,x_sol,n,params,kp);
xdefinedapprox = @(x) w_n(x,coefs,n,params,kp);

% Plot w_n and u
x = linspace(a,b,200);
figure(1)
hold on
%plot(x,real(u(x)), 'DisplayName', 'Target');
plot(x,real(xdefinedapprox(x)),'.-','DisplayName', 'OG');
plot(x,real(xsolapprox(x)),'DisplayName', 'approx');
hold off
%title('')
subtitle("f(x)=1, interval:(" + a+","+b+"), k="+k+", n="+n)
legend show
axis padded
grid on


% Define true solution u(x) for f=1:
u_g = @(x) (1/(2*k^2))*(-2 + exp(1i*k*(x+1)) + exp(-1i*k*(x-1)));
w = @(x) (-exp(1i*k)/(2i*k)).*(g_minus*exp(1i*k*x) + g_plus*exp(-1i*k*x));
u = @(x) u_g(x) + w(x);
% 
u_prime = @(x) (-exp(1i*k)/2)*(g_minus*exp(-1i*k*x) -g_plus*exp(-1i*k))+ (1i*k^3)/2*(exp(1i*k*(x+1)) - exp(-1i*k*(x-1)));


% % Plot w_n and u
% x = linspace(a,b,200);
% figure(1)
% hold on
% plot(x,real(w_n(x,x_sol,n,params,kp)), 'DisplayName', 'Approx');
% %plot(x,real(u(x)), 'DisplayName', 'Target');
% plot(x,real(definedapprox(x,x_sol,params,kp,n)),'DisplayName', 'Target');
% hold off
% %title('')
% subtitle("f(x)=1, interval:(" + a+","+b+"), k="+k+", n="+n)
% legend show
% axis padded
% grid on

% figure(2)
% hold on
% plot(x,u_prime(x), 'DisplayName', 'Target derivative');
% 
% x = x_min: (x_max-x_min)/200 : x_max;
% for ix = 1 : length(x)
%     y(ix) = w_n_prime(x(ix),c,n,params);
% end
% 
% plot(x, y,'DisplayName','approx')
% subtitle("f(x)=1, interval:(" + x_min+","+x_max+"), k="+k+", n="+n)
% axis padded
% grid on
% legend show
% hold off
% 
% 
function res = w_n(x,c,n,params,kp)
temp = 0;
for i=1:2*n+2
   temp = temp + c(i)*ReLU(x,params(i,1),params(i,2),kp);     
end
res = temp;
end


% function res = w_n_prime(x,c,n,params)
% temp = 0;
% for i=1:2*n
%    temp = temp + c(i).*ReLUPrime(x,params(i,1),params(i,2),2);     
% end
% res = temp;
% end

 
% function res = w_n(x,c,n)
% temp = 0;
% for i=1:2*n
%    if i <= n
%        w_i = 1;
%        b_i = -2 + 4*(i-1)/n;
%        if x < -b_i/w_i
%            continue
%        end
%    else
%        w_i = -1;
%        b_i = -2 + 4*(2*n-i)/n;
%        if x > -b_i/w_i
%            continue
%        end
%    end
%    temp = temp + c(i).*ReLU(x,w_i,b_i,2);     
% end
% res = temp;
% end

 
% function res = w_n_prime(x)
% temp = 0;
% for i=1:2*n
%    if i <= n
%        w_i = 1;
%        b_i = -2 + 4*(i-1)/n;
%        if x < -b_i/w_i
%            continue
%        end
%    else
%        w_i = -1;
%        b_i = -2 + 4*(2*n-i)/n;
%        if x > -b_i/w_i
%            continue
%        end
%    end
%    temp = temp + c(i).*ReLUPrime(x,c_i,b_i,2);     
% end
% res = temp;
% end

function res = funct(x,params,kp,k,n)
    temp = 0;
    for i=1:2*n+2
        w = params(i,1);
        nu = params(i,2);
        kappa = kp;
        temp = temp - log(i)*(ReLUPrimePrime(x,w,nu,kappa) + k^(2)*ReLU(x,w,nu,kappa));
    end 
    res = temp;
end 


    
