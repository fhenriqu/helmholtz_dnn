function [res]=w_n_prime(x,c,x_min,x_max,n)
% goal: compute w_n_prime: projection of u_prime in function space, given the coefficients c
range = x_max - x_min;

% function basis: hat functions
phi_prime = @(x) (n/range)*rectangularPulse(-range/n,0,x)...
                - (n/range)*rectangularPulse(0,range/n,x);
phi_last_prime = @(x) (n/range)*rectangularPulse(x_max-range/n,x_max,x);

% computation
temp = 0;
for i=1:n
   if x < x_min +(i-1)*range/n
       continue
   end
   if x> x_min + (i+1)*range/n
       break
   end
   if i==n && n>1
       temp = temp + c(n).*phi_last_prime(x);
       break
   end
   temp = temp + c(i).*phi_prime(x-i*range/n);     
end

res = temp;
