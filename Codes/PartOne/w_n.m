function [res]=w_n(x,c,x_min,x_max,n)
% goal: compute w_n: projection of u in function space, given the coefficients c
range = x_max - x_min;

% function basis: hat functions

phi = @(x) triangularPulse(-range/n,0,range/n,x);
phi_last = @(x) triangularPulse(x_max-range/n,x_max,x_max,x);

% computation
temp = 0;
for i=1:n-1
   if x < x_min +(i-1)*range/n
       continue
   end
   if x> x_min + (i+1)*range/n
       break
   end   
   temp = temp + c(i).*phi(x-i*range/n);     
end
temp = temp + c(n).*phi_last(x);

res = temp;
