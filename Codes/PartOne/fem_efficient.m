clear all
close all

% Starting parameters
x_min = 0;
x_max = 1;
k = 10;
alpha = k;
n = 32; 
range = x_max - x_min;

% define u and u', condition: u(x_min)=0
u = @(x) sin(pi*x);
u_prime = @(x) pi*cos(pi*x);

% define f = -u'' -k^2*u
f = @(x) pi^2 * sin(pi*x) - k^2*u(x);

% define g: complex number satisfies boundary condition: u'(x_max) -i*k*u(x_max) = g
g =  u_prime(x_max) -1i*k*u(x_max)

% define function basis: hat functions
phi = @(x) triangularPulse(-range/n,0,range/n,x);
phi_last = @(x) triangularPulse(x_max-range/n,x_max,x_max+range/n,x);
% derivatives
phi_prime = @(x) (n/range)*rectangularPulse(-range/n,0,x)...
                - (n/range)*rectangularPulse(0,range/n,x);
phi_last_prime = @(x) (n/range)*rectangularPulse(x_max-range/n,x_max,x);

% calculate A(i,j) = a(phi_i,phi_j) =
% we have A(i,j) = 0 for |i-j| >= 2
A = sparse(n, n);

diag = 2*n/range - k^(2)*(2/3)*range/n;
diag_last = -n/range - k^(2)*(1/3)*(range/n) -1i*k;
side = -n/range - k^(2)*(1/6)*range/n;

for i=1:n-1
    A(i,i) = diag;
    A(i,i+1) = side;
    A(i+1,i) = side;
end
A(n,n) = diag_last;
if n==1
    A(n,n) = diag;
end
A
    
% calculate vector b in A*c = b, b_i = l(phi_i)
b = zeros (n,1);
for i=1:n-1
   product = @(x) f(x).*phi(x-i*range/n);
   b(i,1) = integral(product,(i-1)*range/n,(i+1)*range/n);
end
product = @(x) f(x).*phi_last(x);
b(n,1) = integral(product,x_max-range/n,x_max) + g;
b

% solve for c in A*c=b
c = A\b;
c








% % 
% err1=0;
% product = @(x) (abs(u(x)-w_n(x,c,x_min,x_max,n))).^2;
%   for i=1:n
%     %product = @(x) (abs(u_prime(x)-(c(i-1)*phi(x-(i-1)*range/n) +c(i).*phi(x-i*range/n)))).^2;
%     err1 = err1 + integral(product,(i-1)*(range/n)+eps,i*(range/n)-eps)
%   end
%   err1=sqrt(err1)

% i=2
% u =  integral(product,(i-1)*(range/n)+eps,i*(range/n)-eps)

% % L2 error
% err1 = 0;
% % product = @(x) (abs(u_prime(x)-c(1)*phi(x-range/n))).^2;
% % err1 = integral(product,eps, range/n -eps)
% product = @(x) (abs(u(x)-w_n(x,c,x_min,x_max,n))).^2;
% for i=1:n
%     %product = @(x) (abs(u_prime(x)-(c(i-1)*phi(x-(i-1)*range/n) +c(i).*phi(x-i*range/n)))).^2;
%     u =  integral(product,(i-1)*(range/n)+eps,i*(range/n)-eps)
%     err1 = err1 + integral(product,(i-1)*(range/n)+eps,i*(range/n)-eps)
% end
% L2part = sqrt(err1)


% 
% % H1 error
% err2 = 0;
% % product = @(x) (abs(u_prime(x)-c(1)*phi(x-range/n))).^2;
% % err1 = integral(product,eps, range/n -eps)
% product = @(x) (abs(u_prime(x)-w_n_prime(x,c,x_min,x_max,n))).^2;
% for i=1:n
%     %product = @(x) (abs(u_prime(x)-(c(i-1)*phi(x-(i-1)*range/n) +c(i).*phi(x-i*range/n)))).^2;
%     err2 = err2 + integral(product,(i-1)*(range/n)+eps,i*(range/n)-eps)
% end
% H1 = L2 + sqrt(err2)

% err2 = 0;
% slope = n/range;
% product = @(x) (abs(u_prime(x)-c(1)*slope)).^2;
% err2 = integral(product,0+eps,range/n -eps);
% for i=2:n
%     product = @(x) abs(u_prime(x) -(c(i)*slope -c(i-1).*slope)).^2;
%     err2 = err2 + integral(product,(i-1)*range/n + eps,i*range/n -eps); 
% end
% H1 = L2 + sqrt(err2)


% L2 error:
% error = @(x) (abs(u(x)-w_n(x,c,x_min,x_max,n))).^2;
% err1 = integral(error,x_min,x_max);
% L2other = sqrt(err1)

% % 
% % H1 error:
% % error = @(x) (abs(u_prime(x)-w_n_prime(x,c,x_min,x_max,n))).^2;
% % err2 = integral(error,x_min,x_max);
% % H1 = L2 + sqrt(err2)


%plot w_n and u
x = linspace(x_min,x_max,1000);

figure(1)
hold on
plot(x,w_n(x,c,x_min,x_max,n));
plot(x,u(x));
hold off
%title('')
subtitle('u(x)=sin(pi\dotx), interval:(0,1), k=10, n=32')
legend('approximation', 'target')
axis padded
grid on

figure(2)
hold on
plot(x,w_n_prime(x,c,x_min,x_max,n));
plot(x,u_prime(x));
hold off
%title('title')
subtitle('u(x)=sin(pi\dotx), interval:(0,1), k=10, n=32')
legend('approximation', 'target')
axis padded
grid on


