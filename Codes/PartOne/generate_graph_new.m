clear all
clc

n = 11;

x = 2.^linspace(1,n,n);
y1 = 1./x;
y2 = 1./(x.^2);

% errors = zeros(n,2)
for i=1:n
    i
    [L2,H1] = return_errors(2^i);
    errors(i,1)=L2;
    errors(i,2)=H1;
end



 
% Calculated values

L2 = [errors(:,1)];
H1 = [errors(:,2)];


% plot
figure
loglog(x,L2,'-o',x,H1,'-o',x,y2,'k',x,y1,'--k')
xlabel('mesh size')
ylabel('Error')
title('L2 and H1 error vs mesh size')
subtitle({'u(x)=sin(pi*x), interval:(0,1), k=1'})
legend({'L2 error','H1 error','1/x^2','1/x'})
grid on

p=polyfit(log(x),log(L2),1);
slope_L2=p(1)
p=polyfit(log(x),log(H1),1);
slope_H1=p(1)

